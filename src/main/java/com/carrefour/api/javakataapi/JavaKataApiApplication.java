package com.carrefour.api.javakataapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaKataApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaKataApiApplication.class, args);
	}

}
