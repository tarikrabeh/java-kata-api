package com.carrefour.api.javakataapi.resources;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.carrefour.api.javakataapi.entities.DeliveryMode;
import com.carrefour.api.javakataapi.services.DeliveryModeService;

@RestController
@RequestMapping("/api/v1/delivery-modes")
public class DeliveryModeResource {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DeliveryModeResource.class);

	@Autowired
	DeliveryModeService deliveryModeService;
	
	
	@GetMapping()
	public List<DeliveryMode> getDeliveryModes() {
		LOGGER.info("get delivery mode list");
		return deliveryModeService.getDeliveryModes();
	}

}
