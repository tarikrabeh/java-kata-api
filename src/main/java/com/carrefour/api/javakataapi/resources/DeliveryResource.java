package com.carrefour.api.javakataapi.resources;

import org.apache.coyote.BadRequestException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.carrefour.api.javakataapi.entities.Delivery;
import com.carrefour.api.javakataapi.services.DeliveryService;


@RestController
@RequestMapping("/api/v1/deliveries")
public class DeliveryResource {
	private static final Logger LOGGER = LoggerFactory.getLogger(DeliveryResource.class);

	@Autowired
	DeliveryService deliveryService;
	
	@PostMapping()
	Delivery saveDelivery(@RequestBody Delivery delivery) throws NoHandlerFoundException, BadRequestException{
		LOGGER.info("save delivery");
		return deliveryService.createDelivery(delivery);
	}

	@PutMapping("/{id}")
	Delivery updateDelivery(@RequestBody Delivery delivery, @PathVariable Long id) throws NoHandlerFoundException{
		LOGGER.info("save delivery");
		return deliveryService.updateDelivery(id, delivery);
	}
}
