package com.carrefour.api.javakataapi.resources;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.carrefour.api.javakataapi.entities.CreneauHoraire;
import com.carrefour.api.javakataapi.services.CreneauHoraireService;

@RestController
@RequestMapping("/api/v1/creneaux-horaire")
public class CreneauHoraireResource {

	private static final Logger LOGGER = LoggerFactory.getLogger(CreneauHoraireResource.class);
	
	@Autowired
	CreneauHoraireService creneauHoraireService;
	
	@GetMapping("/by-delivery-mode/{deliveryModeId}")
	public List<CreneauHoraire> getCreneauxHoraireByDeliveryMode(@PathVariable Long deliveryModeId) throws NoHandlerFoundException {
		LOGGER.info("get creneaux horaire list");
		return creneauHoraireService.getCreneauHorairesByDeliveryMode(deliveryModeId);
	}

	@GetMapping("{id}")
	public CreneauHoraire getCreneauxHoraireById(@PathVariable Long id) throws NoHandlerFoundException {
		LOGGER.info("get creneaux horaire by id");
		return creneauHoraireService.getCreneauHoraireById(id);
	}
}
