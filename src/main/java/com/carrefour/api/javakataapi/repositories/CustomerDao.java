package com.carrefour.api.javakataapi.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.carrefour.api.javakataapi.entities.Customer;


public interface CustomerDao extends JpaRepository<Customer, Long> {

}
