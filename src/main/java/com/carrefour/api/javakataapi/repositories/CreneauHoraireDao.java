package com.carrefour.api.javakataapi.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.carrefour.api.javakataapi.entities.CreneauHoraire;
import com.carrefour.api.javakataapi.entities.DeliveryMode;

public interface CreneauHoraireDao extends JpaRepository<CreneauHoraire, Long> {

	List<CreneauHoraire> findByDeliveryMode(DeliveryMode deliveryMode);

}
