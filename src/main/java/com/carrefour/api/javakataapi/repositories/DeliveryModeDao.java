package com.carrefour.api.javakataapi.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.carrefour.api.javakataapi.entities.DeliveryMode;


public interface DeliveryModeDao extends JpaRepository<DeliveryMode, Long>{

}
