package com.carrefour.api.javakataapi.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.carrefour.api.javakataapi.entities.Delivery;


public interface DeliveryDao extends JpaRepository<Delivery, Long>{

}
