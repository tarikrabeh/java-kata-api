package com.carrefour.api.javakataapi.entities;

import java.time.LocalDateTime;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name="DELIVERY")
public class Delivery {

	@Id
    @GeneratedValue
    private Long id;
	
	@ManyToOne
    @JoinColumn(name="CUSTOMER_ID")
	Customer customer;
	
	@ManyToOne
    @JoinColumn(name="DELIVERY_MODE_ID")
	DeliveryMode deliveryMode;

	@ManyToOne
    @JoinColumn(name="CRENEAU_HORAIRE_ID", nullable=true)
	CreneauHoraire creneauHoraire;
	
	@Column(nullable = true)
	String jour;
	
	@Column
	LocalDateTime creationDate;
	
	@Column(nullable=true)
	LocalDateTime modificationDate;

	public Delivery() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public DeliveryMode getDeliveryMode() {
		return deliveryMode;
	}

	public void setDeliveryMode(DeliveryMode deliveryMode) {
		this.deliveryMode = deliveryMode;
	}

	public LocalDateTime getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDateTime creationDate) {
		this.creationDate = creationDate;
	}

	public String getJour() {
		return jour;
	}

	public void setJour(String jour) {
		this.jour = jour;
	}

	public CreneauHoraire getCreneauHoraire() {
		return creneauHoraire;
	}

	public void setCreneauHoraire(CreneauHoraire creneauHoraire) {
		this.creneauHoraire = creneauHoraire;
	}

	public LocalDateTime getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(LocalDateTime modificationDate) {
		this.modificationDate = modificationDate;
	}
	
}
