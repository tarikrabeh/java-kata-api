package com.carrefour.api.javakataapi.entities.enums;

public enum Jours {
	LUNDI,
	MARDI,
	MERCREDI,
	JEUDI,
	VENDREDI,
	SAMEDI,
	DIMANCHE
}
