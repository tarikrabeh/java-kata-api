package com.carrefour.api.javakataapi.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.carrefour.api.javakataapi.entities.Customer;
import com.carrefour.api.javakataapi.repositories.CustomerDao;


@Service
public class CustomerService {

	@Autowired
	CustomerDao customerDao;

	public Customer getCustomerById(Long id) {
		Optional<Customer> optCustomer = customerDao.findById(id);
		Customer customer = null;
		if(optCustomer.isPresent()) {
			customer = optCustomer.get();
		}
		return customer;
	}
	
}
