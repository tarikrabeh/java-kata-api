package com.carrefour.api.javakataapi.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.carrefour.api.javakataapi.entities.CreneauHoraire;
import com.carrefour.api.javakataapi.entities.DeliveryMode;
import com.carrefour.api.javakataapi.repositories.CreneauHoraireDao;

@Service
public class CreneauHoraireService {
	
	@Autowired
	CreneauHoraireDao creneauHoraireDao;
	
	@Autowired
	DeliveryModeService deliveryModeService;
	
	public List<CreneauHoraire> getCreneauHorairesByDeliveryMode(Long deliveryModeId) throws NoHandlerFoundException {
		DeliveryMode deliveryMode = deliveryModeService.getDeliveryModeById(deliveryModeId);
		if (deliveryMode == null) {
			throw new NoHandlerFoundException("delivery Mode not found with Row ID: " + deliveryModeId , "",new HttpHeaders());
		}
		return creneauHoraireDao.findByDeliveryMode(deliveryMode);
	}
	
	public CreneauHoraire getCreneauHoraireById(Long id) {
		Optional<CreneauHoraire> optCreneauHoraire = creneauHoraireDao.findById(id);
		CreneauHoraire creneauHoraire = null;
		if(optCreneauHoraire.isPresent()) {
			creneauHoraire = optCreneauHoraire.get();
		}
		return creneauHoraire;
	}

}
