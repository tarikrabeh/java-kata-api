package com.carrefour.api.javakataapi.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.carrefour.api.javakataapi.entities.DeliveryMode;
import com.carrefour.api.javakataapi.repositories.DeliveryModeDao;

@Service
public class DeliveryModeService {
	
	@Autowired
	DeliveryModeDao deliveryModeDao;

	public List<DeliveryMode> getDeliveryModes() {
		return deliveryModeDao.findAll();
	}

	public DeliveryMode getDeliveryModeById(Long id) {
		Optional<DeliveryMode> optDeliveryMode = deliveryModeDao.findById(id);
		DeliveryMode deliveryMode = null;
		if(optDeliveryMode.isPresent()) {
			deliveryMode = optDeliveryMode.get();
		}
		return deliveryMode;
	}

}
