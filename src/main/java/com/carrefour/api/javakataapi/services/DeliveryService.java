package com.carrefour.api.javakataapi.services;

import java.time.LocalDateTime;
import java.util.Optional;

import org.apache.coyote.BadRequestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.carrefour.api.javakataapi.entities.CreneauHoraire;
import com.carrefour.api.javakataapi.entities.Customer;
import com.carrefour.api.javakataapi.entities.Delivery;
import com.carrefour.api.javakataapi.entities.DeliveryMode;
import com.carrefour.api.javakataapi.repositories.DeliveryDao;



@Service
public class DeliveryService {

	@Autowired
	DeliveryDao deliveryDao;
	
	@Autowired
	DeliveryModeService deliveryModeService;
	
	@Autowired
	CustomerService customerService;
	
	@Autowired
	CreneauHoraireService creneauHoraireService;
	
	
	public Delivery getDeliveryById(Long id) {
		Optional<Delivery> optDelivery = deliveryDao.findById(id);
		Delivery delivery = null;
		if(optDelivery.isPresent()) {
			delivery = optDelivery.get();
		}
		return delivery;
	}
	
	public Delivery createDelivery(Delivery delivery) throws NoHandlerFoundException, BadRequestException {
		Delivery newDelivery = new Delivery();
		
		if(delivery.getCustomer() != null) {
			Customer customer = customerService.getCustomerById(delivery.getCustomer().getId());
			
			if (customer == null) {
				throw new NoHandlerFoundException("customer not found with Row ID: " + delivery.getCustomer().getId() , "",new HttpHeaders());
			}
			newDelivery.setCustomer(customer);
		}else {
			throw new BadRequestException("customer is mandatory");
		}

		if(delivery.getDeliveryMode() != null) {
			DeliveryMode deliveryMode = deliveryModeService.getDeliveryModeById(delivery.getDeliveryMode().getId());
			
			if (deliveryMode == null) {
				throw new NoHandlerFoundException("delivery Mode not found with Row ID: " + delivery.getDeliveryMode().getId() , "",new HttpHeaders());
			}
			newDelivery.setDeliveryMode(deliveryMode);
		}else {
			throw new BadRequestException("delivery mode is mandatory");
		}
		
		newDelivery.setCreationDate(LocalDateTime.now());
		return deliveryDao.save(newDelivery);
	}

	public Delivery updateDelivery(Long id, Delivery delivery) throws NoHandlerFoundException {
		Delivery updateDelivery = new Delivery();
		
		if(delivery != null) {
			updateDelivery = getDeliveryById(delivery.getId());
			
			if (updateDelivery == null) {
				throw new NoHandlerFoundException("delivery not found with Row ID: " + delivery.getId() , "",new HttpHeaders());
			}
		}
		
		DeliveryMode deliveryMode = updateDelivery.getDeliveryMode();
		
		if(!"DELIVERY_ASAP".equals(deliveryMode.getName())) {
			if(delivery.getCreneauHoraire() != null) {
				CreneauHoraire creneauHoraire = creneauHoraireService.getCreneauHoraireById(delivery.getCreneauHoraire().getId());
				
				if (creneauHoraire == null) {
					throw new NoHandlerFoundException("creneau horaire not found with Row ID: " + delivery.getCreneauHoraire().getId() , "",new HttpHeaders());
				}
				updateDelivery.setCreneauHoraire(creneauHoraire);
			}
			
			updateDelivery.setJour(delivery.getJour());
		}
		
		updateDelivery.setModificationDate(LocalDateTime.now());
		
		return deliveryDao.save(updateDelivery);
	}


}
