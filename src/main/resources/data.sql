insert into customer (id, name) values (1, 'cutomer1');
insert into customer (id, name) values (2, 'cutomer2');
insert into delivery_mode (id, name, description) values (1, 'DRIVE', 'drive mode');
insert into delivery_mode (id, name, description) values (2, 'DELIVERY', 'delivery mode');
insert into delivery_mode (id, name, description) values (3, 'DELIVERY_TODAY', 'delivery today mode');
insert into delivery_mode (id, name, description) values (4, 'DELIVERY_ASAP', 'delivery asap mode');